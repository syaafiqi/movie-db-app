package id.syaafiqi.movie_db_app.core.storage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import id.syaafiqi.movie_db_app.BuildConfig
import id.syaafiqi.movie_db_app.core.storage.tables.Favorites

private const val DATABASE_VERSION = 2

private val TABLES: List<StorageTable<*>> = listOf(
    Favorites
)

interface StorageTable<T> {
    fun createTable(db: SQLiteDatabase)
    fun dropTable(db: SQLiteDatabase)
    fun get(db: SQLiteDatabase): List<T>
    fun findOne(db: SQLiteDatabase, id: Int): T?
    fun create(db: SQLiteDatabase, data: T)
    fun update(db: SQLiteDatabase, id: Int, data: T)
    fun delete(db: SQLiteDatabase, id: Int)
}

fun Context.storage(): SQLiteOpenHelper =
    object : SQLiteOpenHelper(this, BuildConfig.APPLICATION_ID, null, DATABASE_VERSION) {
        override fun onCreate(db: SQLiteDatabase?) {
            when {
                db != null -> {
                    TABLES.map {
                        it.createTable(db)
                    }
                }
            }
            db?.close()
        }

        override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
            when {
                db != null -> {
                    TABLES.map {
                        it.dropTable(db)
                    }
                }
            }
            onCreate(db)
        }
    }

inline operator fun <reified T : Any> SQLiteOpenHelper.get(table: StorageTable<T>): List<T> {
    val db = this.writableDatabase
    val result = table.get(db)
    db.close()
    return result
}

inline fun <reified T : Any> SQLiteOpenHelper.find(table: StorageTable<T>, id: Int): T? {
    val db = this.writableDatabase
    val result = table.findOne(db, id)
    db.close()
    return result
}

inline fun <reified T : Any> SQLiteOpenHelper.insert(table: StorageTable<T>, data: T) {
    val db = this.writableDatabase
    val result = table.create(db, data)
    db.close()
    return result
}

inline fun <reified T : Any> SQLiteOpenHelper.update(table: StorageTable<T>, id: Int, data: T) {
    val db = this.writableDatabase
    val result = table.update(db, id, data)
    db.close()
    return result
}

inline fun <reified T : Any> SQLiteOpenHelper.delete(table: StorageTable<T>, id: Int) {
    val db = this.writableDatabase
    val result = table.delete(db, id)
    db.close()
    return result
}