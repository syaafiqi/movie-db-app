package id.syaafiqi.movie_db_app.core.storage.tables

import android.annotation.SuppressLint
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import id.syaafiqi.movie_db_app.core.storage.StorageTable

data class FavoritesModel(
    val id: Int,
    val title: String,
    val language: String,
    val rating: Double,
    val ratingCount: Int,
    val releaseDate: String,
    val imageUrl: String
)

object Favorites : StorageTable<FavoritesModel> {
    private val tableName = this::class.java.simpleName
    private const val keyId = "id"
    private const val keyName = "name"
    private const val keyRating = "rating"
    private const val keyRatingCount = "ratingCount"
    private const val keyLanguage = "language"
    private const val keyReleaseDate = "releaseDate"
    private const val keyImageUrl = "imageUrl"

    override fun createTable(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE $tableName (" +
                    "$keyId INTEGER PRIMARY KEY," +
                    "$keyName TEXT," +
                    "$keyRating REAL," +
                    "$keyRatingCount INTEGER," +
                    "$keyLanguage TEXT," +
                    "$keyReleaseDate TEXT," +
                    "$keyImageUrl TEXT" +
                    ")"
        )
    }

    override fun dropTable(db: SQLiteDatabase) {
        db.execSQL("DROP TABLE IF EXISTS $tableName")
    }

    @SuppressLint("Range")
    override fun get(db: SQLiteDatabase): List<FavoritesModel> {
        val result: MutableList<FavoritesModel> = mutableListOf()
        val cursor = db.rawQuery("SELECT * FROM $tableName ORDER BY $keyId", null)
        while (cursor.moveToNext()) result += FavoritesModel(
            id = cursor.getInt(cursor.getColumnIndex(keyId)),
            title = cursor.getString(cursor.getColumnIndex(keyName)),
            rating = cursor.getDouble(cursor.getColumnIndex(keyRating)),
            ratingCount = cursor.getInt(cursor.getColumnIndex(keyRatingCount)),
            language = cursor.getString(cursor.getColumnIndex(keyLanguage)),
            releaseDate = cursor.getString(cursor.getColumnIndex(keyReleaseDate)),
            imageUrl = cursor.getString(cursor.getColumnIndex(keyImageUrl))
        )
        cursor.close()
        return result
    }

    @SuppressLint("Range")
    override fun findOne(db: SQLiteDatabase, id: Int): FavoritesModel? {
        var result: FavoritesModel? = null
        val cursor = db.rawQuery("SELECT * FROM $tableName WHERE id = $id", null)
        if (cursor.count > 0) {
            if (cursor.moveToFirst()) {
                result = FavoritesModel(
                    id = cursor.getInt(cursor.getColumnIndex(keyId)),
                    title = cursor.getString(cursor.getColumnIndex(keyName)),
                    rating = cursor.getDouble(cursor.getColumnIndex(keyRating)),
                    ratingCount = cursor.getInt(cursor.getColumnIndex(keyRatingCount)),
                    language = cursor.getString(cursor.getColumnIndex(keyLanguage)),
                    releaseDate = cursor.getString(cursor.getColumnIndex(keyReleaseDate)),
                    imageUrl = cursor.getString(cursor.getColumnIndex(keyImageUrl))
                )
            }
        }
        cursor.close()
        return result
    }

    override fun create(db: SQLiteDatabase, data: FavoritesModel) {
        val values = ContentValues().apply {
            put(keyId, data.id)
            put(keyName, data.title)
            put(keyRating, data.rating)
            put(keyRatingCount, data.ratingCount)
            put(keyLanguage, data.language)
            put(keyReleaseDate, data.releaseDate)
            put(keyImageUrl, data.imageUrl)
        }
        db.insert(tableName, null, values)
        db.close()
    }

    override fun update(db: SQLiteDatabase, id: Int, data: FavoritesModel) {
        val values = ContentValues().apply {
            put(keyName, data.title)
            put(keyRating, data.rating)
            put(keyRatingCount, data.ratingCount)
            put(keyLanguage, data.language)
            put(keyReleaseDate, data.releaseDate)
            put(keyImageUrl, data.imageUrl)
        }
        db.update(tableName, values, "$keyId=?", arrayOf("$id"))
        db.close()
    }

    override fun delete(db: SQLiteDatabase, id: Int) {
        db.delete(tableName, "$keyId=?", arrayOf("$id"))
        db.close()
    }
}