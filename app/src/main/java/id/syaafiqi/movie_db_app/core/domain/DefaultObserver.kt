package id.syaafiqi.movie_db_app.core.domain

interface DefaultObserver<T : Any> {
    fun onSuccess(entity: T)
    fun onError(exception: Throwable)
}