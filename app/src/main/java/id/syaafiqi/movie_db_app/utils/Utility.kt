package id.syaafiqi.movie_db_app.utils

import android.content.Context
import android.os.Build
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale

object Utility {
    fun createLoaderPlaceholder(context: Context): CircularProgressDrawable {
        val loaderPlaceholder = CircularProgressDrawable(context)
        with(loaderPlaceholder) {
            strokeWidth = 10f
            centerRadius = 50f
            start()
        }
        return loaderPlaceholder
    }

    fun reformatDateString(input: String, inputFormat: String = "yyyy-MM-dd", outputFormat: String = "dd MMM yyyy"): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LocalDate.parse(input, DateTimeFormatter.ofPattern(inputFormat)).format(
                DateTimeFormatter.ofPattern(outputFormat))
        } else {
            val date = SimpleDateFormat(inputFormat, Locale.ROOT).parse(input)
            return SimpleDateFormat(outputFormat, Locale.ROOT).format(date ?: Date())
        }
    }
}