package id.syaafiqi.movie_db_app.utils

object Constants {
    const val MOVIE_INFO: String = "MOVIE_INFO"
    const val POP_BACK_STACK_STATE = "root_fragment"
    object VideoProps {
        const val YOUTUBE: String = "YOUTUBE"
        const val TRAILER: String = "TRAILER"
        const val TEASER: String = "TEASER"
    }
    const val ACTION_BAR_TITLE_SHOW_VALUE: Int = 850
}