package id.syaafiqi.movie_db_app.features.view_movie

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.movie_db_app.R
import id.syaafiqi.movie_db_app.databinding.ItemLoadingBinding
import id.syaafiqi.movie_db_app.databinding.ItemMovieBinding
import id.syaafiqi.movie_db_app.databinding.ItemReviewBinding
import id.syaafiqi.movie_db_app.extensions.load
import id.syaafiqi.movie_db_app.extensions.show

class MovieReviewAdapter(
    private var reviewList: ArrayList<MovieReviewModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoading = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> LoadMoreViewHolder(
                ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            else -> MovieViewHolder(
                ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }

    @SuppressLint("NotifyDataSetChanged")
    inner class MovieViewHolder(private val view: ItemReviewBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: MovieReviewModel) {
            with(view) {
                ivThumbnail.load(data.profileImageUrl)
                tvAuthor.text = data.author
                tvContent.text = data.content
            }

        }
    }

    inner class LoadMoreViewHolder(private val view: ItemLoadingBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            with(view) {
                loadingStateRv.show()
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when {
        position == reviewList.size && isLoading -> 0
        else -> 1
    }

    override fun getItemCount(): Int = when (isLoading) {
        true -> reviewList.size + 1
        else -> reviewList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoadMoreViewHolder -> holder.bind()
            is MovieViewHolder -> holder.bind(reviewList[position])
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addItems(addedItems: ArrayList<MovieReviewModel>, isLastPage: Boolean = false) {
        isLoading = !isLastPage
        reviewList.addAll(addedItems)
        notifyDataSetChanged()
    }

    interface ViewAction {}
}