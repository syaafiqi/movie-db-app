package id.syaafiqi.movie_db_app.features.view_movie

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.*
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.movie_db_app.R
import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.core.storage.delete
import id.syaafiqi.movie_db_app.core.storage.find
import id.syaafiqi.movie_db_app.core.storage.insert
import id.syaafiqi.movie_db_app.core.storage.storage
import id.syaafiqi.movie_db_app.core.storage.tables.Favorites
import id.syaafiqi.movie_db_app.core.storage.tables.FavoritesModel
import id.syaafiqi.movie_db_app.databinding.ActivityViewMovieBinding
import id.syaafiqi.movie_db_app.databinding.FragmentReviewsBinding
import id.syaafiqi.movie_db_app.extensions.*
import id.syaafiqi.movie_db_app.features.home.MovieModel
import id.syaafiqi.movie_db_app.utils.Constants
import id.syaafiqi.movie_db_app.utils.CustomAppBarLayoutBehavior
import kotlin.math.abs


class ViewMovieActivity : AppCompatActivity() {
    private val observer: ViewMovieObserver = ViewMovieObserver()
    private var presenter: ViewMoviePresenter = ViewMoviePresenter(observer)
    private lateinit var binding: ActivityViewMovieBinding
    private var movieInfo: MovieModel? = null

    // Dialog components
    private lateinit var dialog: Dialog
    private lateinit var dialogBinding: FragmentReviewsBinding
    private val dialogPaging = PagedRequest()

    // AppBar properties
    private var statusBarDefault: Int = 0
    var expandedActionBar = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewMovieBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_AppCompat_Light_NoActionBar)
        setContentView(binding.root)

        movieInfo = getExtra(Constants.MOVIE_INFO)
        dialog = Dialog(this)
        dialogBinding = FragmentReviewsBinding.inflate(layoutInflater)

        initView()
    }

    private fun initView() {
        setSupportActionBar(binding.toolbar)
        statusBarDefault = this.window.statusBarColor

        binding.appBar.addOnOffsetChangedListener { _, p1 ->
            if (abs(p1) > Constants.ACTION_BAR_TITLE_SHOW_VALUE) {
                expandedActionBar = false;
                binding.cToolbar.title = movieInfo?.title ?: ""
                this.window.statusBarColor = getColor(R.color.purple_500)
                invalidateOptionsMenu();
            } else {
                expandedActionBar = true;
                binding.cToolbar.title = getString(R.string.empty_string)
                this.window.statusBarColor = statusBarDefault
                invalidateOptionsMenu();
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.findMovie(movieInfo?.id ?: -1)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_view_movie, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favorite -> favoriteAction()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val existingFavorite = storage().find(Favorites, movieInfo?.id ?: -1)
        existingFavorite?.let {
            menu?.findItem(R.id.favorite)?.icon =
                AppCompatResources.getDrawable(this, R.drawable.ic_favorite_active_24)
        } ?: run {
            menu?.findItem(R.id.favorite)?.icon =
                AppCompatResources.getDrawable(
                    this@ViewMovieActivity,
                    R.drawable.ic_favorite_inactive_24
                )
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        when (binding.wvYoutube.visibility) {
            View.VISIBLE -> {
                binding.wvYoutube.loadUrl("about:blank")
                binding.wvYoutube.hide()
                ((binding.appBar.layoutParams as CoordinatorLayout.LayoutParams).behavior as CustomAppBarLayoutBehavior).setScrollBehavior(
                    true
                )
            }
            else -> onBackPressedDispatcher.onBackPressed()
        }
        return true
    }

    private fun favoriteAction() {
        movieInfo?.let { movie ->
            val existing = storage().find(Favorites, movie.id)
            // If existing exist then remove
            existing?.let {
                storage().delete(
                    Favorites, it.id
                )
                binding.root.showToast(getString(R.string.removed_from_favorites))
                invalidateOptionsMenu()
            } ?: run {
                // If not exist then insert
                storage().insert(
                    Favorites, FavoritesModel(
                        id = movie.id,
                        title = movie.title,
                        rating = movie.rating,
                        ratingCount = movie.ratingCount,
                        imageUrl = movie.imageUrl,
                        releaseDate = movie.releaseDate,
                        language = movie.language
                    )
                )
                binding.root.showToast(getString(R.string.added_to_favorites))
                invalidateOptionsMenu()
            }
        }
    }

    fun showReviewPopup() {
        dialog.setContentView(dialogBinding.root)

        dialog.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialogBinding.closeBtn.setOnClickListener {
            dialog.dismiss()
        }
        if (dialog.window != null) dialog.window?.attributes?.windowAnimations =
            R.style.SlideUpDialogAnimation

        presenter.getReview(movieInfo?.id ?: -1, dialogPaging)

        dialog.show()
    }

    inner class ViewMovieObserver : ViewMovieView {
        override var isLastPage: Boolean = false
        override var isLoadMore: Boolean = false

        override fun onShowLoading() {
            binding.loadingState.root.show()
        }

        override fun onHideLoading() {
            binding.loadingState.root.hide()
        }

        override fun onLoadSuccess(model: MovieDetailModel) {
            with(binding) {
                ivImage.load(model.posterUrl)
                tvTitle.text = model.title
                tvReleaseDate.text = getString(R.string.release_date_label, model.releaseDate)
                tvPublisher.text = model.productionCompanies.joinToString(", ") { it.name }
                tvRating.text = root.context.getString(
                    R.string.rating_label,
                    model.rating.toString(),
                    model.ratingCount.toString()
                )
                tvRuntime.text = model.runtime
                tvLanguage.text = model.language.uppercase()
                tvDescription.text =
                    HtmlCompat.fromHtml(model.description, HtmlCompat.FROM_HTML_MODE_LEGACY)
                when (model.trailer != null) {
                    true -> {
                        btnPlayTrailer.show()
                        btnPlayTrailer.setOnClickListener {
                            initWebView(wvYoutube)
                            wvYoutube.loadUrl(model.trailer.url)
                            ((appBar.layoutParams as CoordinatorLayout.LayoutParams).behavior as CustomAppBarLayoutBehavior).setScrollBehavior(
                                false
                            )
                        }
                    }
                    else -> btnPlayTrailer.hide()
                }
                btnSeeReviews.setOnClickListener {
                    showReviewPopup()
                }
            }
        }

        override fun onLoadFailed() {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }

        override fun onLoadReviewSuccess(model: List<MovieReviewModel>) {
            dialogBinding.errorState.root.hide()
            dialogBinding.emptyState.root.hide()

            when (model.size) {
                0 -> dialogBinding.emptyState.root.show()
                else -> {
                    dialogBinding.rvReviews.adapter = MovieReviewAdapter(ArrayList(model))
                    setLinearRecyclerView()
                }
            }
        }

        override fun onLoadReviewFailed() {
            with(dialogBinding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    observer.isLastPage = false
                    dialogPaging.page = 1
                    presenter.getReview(movieInfo?.id ?: -1, dialogPaging)
                }
                root.show()
            }
        }

        override fun onFetchReviewSuccess(model: List<MovieReviewModel>) {
            (dialogBinding.rvReviews.adapter as MovieReviewAdapter).addItems(
                ArrayList(model),
                isLastPage
            )
        }

        override fun onFetchReviewFailed() {
            with(dialogBinding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    observer.isLastPage = false
                    dialogPaging.page = 1
                    presenter.getReview(movieInfo?.id ?: -1, dialogPaging)
                }
                root.show()
            }
        }

        @SuppressLint("SetJavaScriptEnabled")
        private fun initWebView(webView: WebView) {
            webView.show()
//            webView.webViewClient = object : WebViewClient() {
//                override fun shouldOverrideUrlLoading(
//                    view: WebView?,
//                    request: WebResourceRequest?
//                ): Boolean {
//                    return false
//                }
//            }
            webView.webChromeClient = object : WebChromeClient() {
                override fun onPermissionRequest(request: PermissionRequest) {
                    runOnUiThread {
                        request.grant(request.resources)
                    }
                }
            }

            val webSettings: WebSettings = webView.settings
            webSettings.javaScriptEnabled = true
            webSettings.loadWithOverviewMode = true
            webSettings.useWideViewPort = true
        }

        private fun setLinearRecyclerView() {
            dialogBinding.rvReviews.layoutManager = LinearLayoutManager(binding.root.context)
            dialogBinding.rvReviews.addItemDecoration(
                DividerItemDecoration(
                    binding.root.context,
                    (dialogBinding.rvReviews.layoutManager as LinearLayoutManager).orientation
                )
            )
            dialogBinding.rvReviews.clearOnScrollListeners()
            dialogBinding.rvReviews.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val layoutManager = dialogBinding.rvReviews.layoutManager as LinearLayoutManager
                    val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount

                    if (!observer.isLastPage && !observer.isLoadMore &&
                        visibleItemCount.plus(firstVisibleItemPosition) >= totalItemCount &&
                        firstVisibleItemPosition >= 0
                    ) {
                        dialogPaging.page++
                        presenter.fetchReview(movieInfo?.id ?: -1, dialogPaging)
                    }
                }
            })
        }
    }

}