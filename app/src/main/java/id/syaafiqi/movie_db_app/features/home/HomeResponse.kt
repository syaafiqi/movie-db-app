package id.syaafiqi.movie_db_app.features.home

import com.google.gson.annotations.SerializedName

data class HomeResponse(
    @SerializedName("total_results")
    val count: Int,
    val results: List<Data>
) {
    data class Data(
        val id: Int,
        val title: String,
        @SerializedName("original_title")
        val titleOriginal: String,
        @SerializedName("original_language")
        val language: String,
        val description: String,
        @SerializedName("release_date")
        val releaseDate: String?,
        @SerializedName("poster_path")
        val imageUrl: String?,
        @SerializedName("vote_average")
        val rating: Double,
        @SerializedName("vote_count")
        val ratingCount: Int,
        @SerializedName("adult")
        val adultContent: Boolean
    )
}
