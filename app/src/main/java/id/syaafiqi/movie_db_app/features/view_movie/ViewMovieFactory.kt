package id.syaafiqi.movie_db_app.features.view_movie

import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.core.domain.DefaultObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ViewMovieFactory(private val instance: ViewMovieDatasource) {
    fun findMovie(id: Int, callback: DefaultObserver<ViewMovieResponse>): Disposable {
        return instance.getMovieDetail(id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }

    fun getReview(id: Int, paging: PagedRequest, callback: DefaultObserver<ReviewResponse>): Disposable {
        return instance.getMovieReview(id, paging.page)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }
}