package id.syaafiqi.movie_db_app.features.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.databinding.FragmentHomeBinding
import id.syaafiqi.movie_db_app.extensions.gotoActivity
import id.syaafiqi.movie_db_app.extensions.hide
import id.syaafiqi.movie_db_app.extensions.show
import id.syaafiqi.movie_db_app.features.view_movie.ViewMovieActivity
import id.syaafiqi.movie_db_app.utils.Constants

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val observer: HomeViewObserver = HomeViewObserver()
    private val presenter: HomePresenter = HomePresenter(observer)
    private lateinit var adapter: HomeAdapter

    private var paging: PagedRequest = PagedRequest()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        setActions()
        initView()

        return binding.root
    }

    private fun initView() {
        paging = PagedRequest()
        presenter.getList(paging)
    }

    private fun setActions() {
        binding.discoveryRefresherPanel.setOnRefreshListener {
            initView()
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                paging.keyword = if (p0.isNullOrBlank()) "" else p0
                paging.page = 1
                observer.isLastPage = false
                presenter.getList(paging)
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean = false
        })
        binding.searchView.setOnCloseListener {
            paging.keyword = ""
            paging.page = 1
            observer.isLastPage = false
            presenter.getList(paging)
            false
        }
    }

//    private fun setLinearRecyclerView() {
//        binding.rvDiscovery.layoutManager = LinearLayoutManager(binding.root.context)
//        binding.rvDiscovery.clearOnScrollListeners()
//        binding.rvDiscovery.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                super.onScrolled(recyclerView, dx, dy)
//
//                val layoutManager = binding.rvDiscovery.layoutManager as LinearLayoutManager
//                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
//                val visibleItemCount = layoutManager.childCount
//                val totalItemCount = layoutManager.itemCount
//
//                if (!observer.isLastPage && !observer.isLoadMore &&
//                    visibleItemCount.plus(firstVisibleItemPosition) >= totalItemCount &&
//                    firstVisibleItemPosition >= 0
//                ) {
//                    paging.page++
//                    presenter.fetchList(paging)
//                }
//            }
//        })
//    }

    private fun setGridRecyclerView() {
        binding.rvDiscovery.layoutManager = GridLayoutManager(binding.root.context, 2)
        binding.rvDiscovery.clearOnScrollListeners()
        binding.rvDiscovery.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
                with(recyclerView.layoutManager as GridLayoutManager) {
                    if (childCount + findFirstVisibleItemPosition() == itemCount && !observer.isLastPage && !observer.isLoadMore) {
                        paging.page++
                        presenter.fetchList(paging)
                    }
                }
            }
        })
        (binding.rvDiscovery.layoutManager as GridLayoutManager).spanSizeLookup =
            object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int =
                    when (adapter.getItemViewType(position)) {
                        0 -> 2
                        else -> 1
                    }
            }
    }

    inner class HomeViewObserver : HomeView {
        override var isLastPage: Boolean = false
        override var isLoadMore: Boolean = false

        override fun onShowLoading() {
            binding.loadingState.root.show()
        }

        override fun onHideLoading() {
            binding.loadingState.root.hide()
            binding.discoveryRefresherPanel.isRefreshing = false
        }

        override fun onLoadSuccess(models: List<MovieModel>) {
            binding.errorState.root.hide()
            binding.emptyState.root.hide()

            when (models.size) {
                0 -> binding.emptyState.root.show()
                else -> {
                    adapter = HomeAdapter(ArrayList(models), RecyclerViewActionObserver())
                    binding.rvDiscovery.adapter = adapter
                    setGridRecyclerView()
                }
            }
        }

        override fun onLoadFailed() {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }

        override fun onFetchSuccess(models: List<MovieModel>) {
            adapter.addItems(ArrayList(models), isLastPage)
        }

        override fun onFetchFailed() {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }
    }

    inner class RecyclerViewActionObserver : HomeAdapter.ViewAction {
        override fun itemClick(data: MovieModel) {
            activity?.gotoActivity(ViewMovieActivity::class, extras = mapOf(Constants.MOVIE_INFO to data))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
        _binding = null
    }
}