package id.syaafiqi.movie_db_app.features.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.syaafiqi.movie_db_app.core.storage.get
import id.syaafiqi.movie_db_app.core.storage.storage
import id.syaafiqi.movie_db_app.core.storage.tables.Favorites
import id.syaafiqi.movie_db_app.core.storage.tables.FavoritesModel
import id.syaafiqi.movie_db_app.databinding.FragmentFavoritesBinding
import id.syaafiqi.movie_db_app.extensions.gotoActivity
import id.syaafiqi.movie_db_app.extensions.hide
import id.syaafiqi.movie_db_app.extensions.show
import id.syaafiqi.movie_db_app.features.home.MovieModel
import id.syaafiqi.movie_db_app.features.view_movie.ViewMovieActivity
import id.syaafiqi.movie_db_app.utils.Constants

class FavoritesFragment: Fragment() {
    private var _binding: FragmentFavoritesBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var favorites: List<FavoritesModel> = emptyList()
    private lateinit var adapter: FavoritesAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)

        initView()
        setActions()

        return binding.root
    }

    private fun initView() {
        try {
            val fetch = context?.storage()?.get(Favorites)
            fetch?.let {
                when(it.size) {
                    0 -> binding.emptyState.root.show()
                    else -> {
                        favorites = fetch
                        adapter = FavoritesAdapter(ArrayList(favorites), RecyclerViewActionObserver())
                        binding.rvMovies.adapter = adapter
                        setGridRecyclerView()
                    }
                }
            } ?: run { binding.emptyState.root.show() }
            binding.moviesRefresherPanel.isRefreshing = false
        }
        catch (ex: Exception) {
            with(binding.errorState) {
                tryAgainBtn.setOnClickListener {
                    root.hide()
                    initView()
                }
                root.show()
            }
        }
    }

    private fun setActions() {
        binding.moviesRefresherPanel.setOnRefreshListener {
            initView()
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                adapter = FavoritesAdapter(ArrayList(favorites.filter { x -> x.title.lowercase().contains(p0?.lowercase() ?: "") }), RecyclerViewActionObserver())
                binding.rvMovies.adapter = adapter
                setGridRecyclerView()
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean = false
        })
        binding.searchView.setOnCloseListener {
            adapter = FavoritesAdapter(ArrayList(favorites), RecyclerViewActionObserver())
            binding.rvMovies.adapter = adapter
            setGridRecyclerView()
            false
        }
    }


//    private fun setLinearRecyclerView() {
//        binding.rvMovies.layoutManager = LinearLayoutManager(binding.root.context)
//    }

    private fun setGridRecyclerView() {
        binding.rvMovies.layoutManager = GridLayoutManager(binding.root.context, 2)
    }

    inner class RecyclerViewActionObserver : FavoritesAdapter.ViewAction {
        override fun itemClick(data: FavoritesModel) {
            activity?.gotoActivity(ViewMovieActivity::class, extras = mapOf(Constants.MOVIE_INFO to MovieModel(
                id = data.id,
                title = data.title,
                imageUrl = data.imageUrl,
                rating = data.rating,
                ratingCount = data.ratingCount,
                releaseDate = data.releaseDate,
                language = data.language
            )))
        }
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}