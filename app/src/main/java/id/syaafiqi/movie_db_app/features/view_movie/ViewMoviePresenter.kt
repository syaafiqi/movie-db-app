package id.syaafiqi.movie_db_app.features.view_movie

import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.core.domain.DefaultObserver
import id.syaafiqi.movie_db_app.services.NetworkProvider
import io.reactivex.disposables.CompositeDisposable

class ViewMoviePresenter(
    private val view: ViewMovieView,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) {
    private val factory: ViewMovieFactory =
        ViewMovieFactory(NetworkProvider.retrofitInstance.create(ViewMovieDatasource::class.java))

    fun findMovie(id: Int) {
        view.onShowLoading()

        val disposable = factory.findMovie(
            id,
            object : DefaultObserver<ViewMovieResponse> {
                override fun onSuccess(entity: ViewMovieResponse) {
                    view.onHideLoading()
                    view.onLoadSuccess(
                        MovieDetailModel.Map.from(entity)
                    )
                }

                override fun onError(exception: Throwable) {
                    view.onHideLoading()
                    view.onLoadFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }

    fun getReview(id: Int, paging: PagedRequest) {
        view.isLoadMore = true

        val disposable = factory.getReview(
            id,
            paging,
            object : DefaultObserver<ReviewResponse> {
                override fun onSuccess(entity: ReviewResponse) {
                    view.isLoadMore = false
                    if (entity.results.isEmpty()) view.isLastPage = true
                    view.onLoadReviewSuccess(entity.results.map { item ->
                        MovieReviewModel.Map.from(item)
                    })
                }

                override fun onError(exception: Throwable) {
                    view.isLoadMore = false
                    view.onLoadReviewFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }

    fun fetchReview(id: Int, paging: PagedRequest) {
        view.isLoadMore = true

        val disposable = factory.getReview(
            id,
            paging,
            object : DefaultObserver<ReviewResponse> {
                override fun onSuccess(entity: ReviewResponse) {
                    view.isLoadMore = false
                    if (entity.results.isEmpty()) view.isLastPage = true
                    view.onFetchReviewSuccess(entity.results.map { item ->
                        MovieReviewModel.Map.from(item)
                    })
                }

                override fun onError(exception: Throwable) {
                    view.isLoadMore = false
                    view.onFetchReviewFailed()
                }
            }
        )

        compositeDisposable.add(disposable)
    }


}
