package id.syaafiqi.movie_db_app.features.view_movie

interface ViewMovieView {
    fun onShowLoading()
    fun onHideLoading()
    fun onLoadSuccess(model: MovieDetailModel)
    fun onLoadFailed()
    fun onLoadReviewSuccess(model: List<MovieReviewModel>)
    fun onLoadReviewFailed()
    fun onFetchReviewSuccess(model: List<MovieReviewModel>)
    fun onFetchReviewFailed()

    var isLastPage: Boolean
    var isLoadMore: Boolean
}