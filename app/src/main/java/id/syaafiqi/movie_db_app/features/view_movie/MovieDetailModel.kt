package id.syaafiqi.movie_db_app.features.view_movie

import id.syaafiqi.movie_db_app.BuildConfig
import id.syaafiqi.movie_db_app.utils.Constants
import id.syaafiqi.movie_db_app.utils.Utility

data class MovieDetailModel(
    val id: Int,
    val title: String,
    val description: String,
    val language: String,
    val releaseDate: String,
    val posterUrl: String,
    val rating: String,
    val ratingCount: String,
    val budget: Double,
    val revenue: Double,
    val runtime: String,
    val status: String,
    val homepage: String?,
    val hasVideo: Boolean,

    val genres: String,
    val productionCompanies: List<ProductionCompany>,
    val trailer: Video?
) {
    data class ProductionCompany(
        val id: Int,
        val name: String,
        val logoUrl: String,
        val originCountry: String
    )

    data class Video(
        val name: String,
        val url: String
    )

    object Map {
        fun from(data: ViewMovieResponse) = MovieDetailModel(
            id = data.id,
            title = data.title,
            description = data.description,
            language = data.language,
            releaseDate = Utility.reformatDateString(data.releaseDate),
            posterUrl = (BuildConfig.IMAGE_BASE_URL + data.posterPath),
            rating = String.format("%.2f", data.rating),
            ratingCount = data.ratingCount.toInt().toString(),
            budget = data.budget,
            revenue = data.revenue,
            runtime = "${data.runtime ?: "0"} minutes",
            status = data.status,
            homepage = data.homepage,
            hasVideo = data.hasVideo,
            genres = data.genres.joinToString(", ") { genre -> genre.name },
            productionCompanies = data.productionCompanies.map { company ->
                ProductionCompany(
                    id = company.id,
                    name = company.name,
                    logoUrl = (BuildConfig.IMAGE_BASE_URL + company.logoPath),
                    originCountry = company.originCountry
                )
            },
            trailer = data.videos.results.find { video -> video.site.uppercase() == Constants.VideoProps.YOUTUBE && video.type.uppercase() == Constants.VideoProps.TRAILER && video.official }
                ?.let {
                    Video(
                        name = it.name,
                        url = BuildConfig.YOUTUBE_EMBED_URL + it.key
                    )
                }
        )
    }
}