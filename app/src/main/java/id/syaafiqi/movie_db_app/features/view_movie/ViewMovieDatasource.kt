package id.syaafiqi.movie_db_app.features.view_movie

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ViewMovieDatasource {
    @GET("movie/{movie_id}")
    fun getMovieDetail(
        @Path("movie_id") id: Int,
        @Query("append_to_response") append: String = "videos"
    ): Observable<ViewMovieResponse>

    @GET("movie/{movie_id}/reviews")
    fun getMovieReview(
        @Path("movie_id") id: Int,
        @Query("page") page: Int
    ): Observable<ReviewResponse>
}