package id.syaafiqi.movie_db_app.features.home

interface HomeView {
    fun onShowLoading()
    fun onHideLoading()
    fun onFetchSuccess(models: List<MovieModel>)
    fun onFetchFailed()
    fun onLoadSuccess(models: List<MovieModel>)
    fun onLoadFailed()

    var isLastPage: Boolean
    var isLoadMore: Boolean
}