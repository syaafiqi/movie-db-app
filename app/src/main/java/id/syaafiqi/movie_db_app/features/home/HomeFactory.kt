package id.syaafiqi.movie_db_app.features.home

import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.core.domain.DefaultObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class HomeFactory(private val instance: HomeDatasource) {
    fun getMoviesDiscoveryList(pagedRequest: PagedRequest, callback: DefaultObserver<HomeResponse>): Disposable {
        return instance.getMoviesDiscovery(pagedRequest.page)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }

    fun searchMovies(pagedRequest: PagedRequest, callback: DefaultObserver<HomeResponse>): Disposable {
        return instance.searchMovies(pagedRequest.page, pagedRequest.keyword)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                }
            )
    }
}