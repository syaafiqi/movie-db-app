package id.syaafiqi.movie_db_app.features.home

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeDatasource {
    @GET("discover/movie")
    fun getMoviesDiscovery(
        @Query("page") page: Int
    ): Observable<HomeResponse>

    @GET("search/movie")
    fun searchMovies(
        @Query("page") page: Int,
        @Query("query") keyword: String
    ): Observable<HomeResponse>
}