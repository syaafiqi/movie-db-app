package id.syaafiqi.movie_db_app.features.home

import android.os.Parcelable
import id.syaafiqi.movie_db_app.BuildConfig
import id.syaafiqi.movie_db_app.core.storage.tables.FavoritesModel
import id.syaafiqi.movie_db_app.features.view_movie.ViewMovieResponse
import id.syaafiqi.movie_db_app.utils.Utility
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieModel(
    val id: Int,
    val title: String,
    val language: String,
    val rating: Double,
    val ratingCount: Int,
    val releaseDate: String,
    val imageUrl: String
) : Parcelable {
    object Map {
        fun from(data: HomeResponse.Data) = MovieModel(
            id = data.id,
            title = data.title,
            language = data.language,
            rating = data.rating,
            ratingCount = data.ratingCount,
            releaseDate = Utility.reformatDateString(data.releaseDate ?: "1900-01-01"),
            imageUrl = (BuildConfig.IMAGE_BASE_URL + data.imageUrl)
        )

        fun from(data: FavoritesModel) = MovieModel(
            id = data.id,
            title = data.title,
            language = "",
            rating = data.rating,
            ratingCount = data.ratingCount,
            releaseDate = Utility.reformatDateString(data.releaseDate),
            imageUrl = (BuildConfig.IMAGE_BASE_URL + data.imageUrl)
        )
    }
}
