package id.syaafiqi.movie_db_app.features.home

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.syaafiqi.movie_db_app.R
import id.syaafiqi.movie_db_app.databinding.ItemLoadingBinding
import id.syaafiqi.movie_db_app.databinding.ItemMovieBinding
import id.syaafiqi.movie_db_app.extensions.load
import id.syaafiqi.movie_db_app.extensions.show

class HomeAdapter(
    private var movieList: ArrayList<MovieModel>,

    private val listener: ViewAction
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoading = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> LoadMoreViewHolder(
                ItemLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
            else -> MovieViewHolder(
                ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }

    @SuppressLint("NotifyDataSetChanged")
    inner class MovieViewHolder(private val view: ItemMovieBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(data: MovieModel) {
            with(view) {
                view.ivThumbnail.load(data.imageUrl)
                tvTitle.text = data.title
                tvReleaseDate.text = root.context.getString(R.string.release_date_label, data.releaseDate)
                tvRating.text = root.context.getString(R.string.rating_label, data.rating.toString(), data.ratingCount.toString())
                view.root.setOnClickListener {
                    listener.itemClick(data)
                }
            }

        }
    }

    inner class LoadMoreViewHolder(private val view: ItemLoadingBinding) :
        RecyclerView.ViewHolder(view.root) {
        fun bind() {
            with(view) {
                loadingStateRv.show()
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when {
        position == movieList.size && isLoading -> 0
        else -> 1
    }

    override fun getItemCount(): Int = when (isLoading) {
        true -> movieList.size + 1
        else -> movieList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoadMoreViewHolder -> holder.bind()
            is MovieViewHolder -> holder.bind(movieList[position])
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addItems(addedItems: ArrayList<MovieModel>, isLastPage: Boolean = false) {
        isLoading = !isLastPage
        movieList.addAll(addedItems)
        notifyDataSetChanged()
    }

    interface ViewAction {
        fun itemClick(data: MovieModel)
    }
}