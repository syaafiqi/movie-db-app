package id.syaafiqi.movie_db_app.features.view_movie

import com.google.gson.annotations.SerializedName

data class ViewMovieResponse(
    val id: Int,
    val title: String,
    @SerializedName("overview")
    val description: String,
    @SerializedName("original_language")
    val language: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("vote_average")
    val rating: Double,
    @SerializedName("vote_count")
    val ratingCount: Double,
    val budget: Double,
    val revenue: Double,
    val runtime: Int?,
    val status: String,
    val homepage: String?,
    @SerializedName("video")
    val hasVideo: Boolean,

    val genres: List<Genre>,
    @SerializedName("production_companies")
    val productionCompanies: List<ProductionCompany>,
    val videos: VideoResponse
) {
    data class ProductionCompany(
        val id: Int,
        val name: String,
        @SerializedName("logo_path")
        val logoPath: String?,
        @SerializedName("origin_country")
        val originCountry: String
    )

    data class Genre(
        val id: Int,
        val name: String
    )
}

data class VideoResponse(
    val results: List<Video>
) {
    data class Video(
        val name: String,
        val key: String,
        val site: String,
        val type: String,
        val size: Int,
        val official: Boolean
    )
}

data class ReviewResponse(
    val results: List<Review>
) {
    data class Review(
        @SerializedName("author_details")
        val author: Author,
        val content: String
    ) {
        data class Author(
            val name: String,
            val username: String,
            @SerializedName("avatar_path")
            val avatarPath: String?,
            val rating: Int?
        )
    }
}
