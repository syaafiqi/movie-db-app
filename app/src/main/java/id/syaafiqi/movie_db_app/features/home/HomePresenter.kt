package id.syaafiqi.movie_db_app.features.home

import id.syaafiqi.movie_db_app.BuildConfig
import id.syaafiqi.movie_db_app.core.data.PagedRequest
import id.syaafiqi.movie_db_app.core.domain.DefaultObserver
import id.syaafiqi.movie_db_app.services.NetworkProvider
import id.syaafiqi.movie_db_app.utils.Utility
import io.reactivex.disposables.CompositeDisposable

class HomePresenter(
    private val view: HomeView,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) {
    private val factory: HomeFactory =
        HomeFactory(NetworkProvider.retrofitInstance.create(HomeDatasource::class.java))

    fun getList(paging: PagedRequest) {
        view.onShowLoading()

        val callback = object : DefaultObserver<HomeResponse> {
            override fun onSuccess(entity: HomeResponse) {

                view.onHideLoading()
                if (entity.results.isEmpty()) view.isLastPage = true
                view.onLoadSuccess(entity.results.map { item ->
                    MovieModel.Map.from(item)
                })
            }

            override fun onError(exception: Throwable) {
                view.onHideLoading()
                view.onLoadFailed()
            }
        }

        val disposable = if (paging.keyword.isEmpty()) factory.getMoviesDiscoveryList(
            paging,
            callback
        ) else factory.searchMovies(paging, callback)

        compositeDisposable.add(disposable)
    }

    fun fetchList(paging: PagedRequest) {
        view.isLoadMore = true

        val callback = object : DefaultObserver<HomeResponse> {
            override fun onSuccess(entity: HomeResponse) {
                view.isLoadMore = false
                if (entity.results.isEmpty()) view.isLastPage = true
                view.onFetchSuccess(entity.results.map { item ->
                    MovieModel.Map.from(item)
                })
            }

            override fun onError(exception: Throwable) {
                view.isLoadMore = false
                view.onFetchFailed()
            }
        }

        val disposable = if (paging.keyword.isEmpty()) factory.getMoviesDiscoveryList(
            paging,
            callback
        ) else factory.searchMovies(paging, callback)

        compositeDisposable.add(disposable)
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }
}