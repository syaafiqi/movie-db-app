package id.syaafiqi.movie_db_app.features.view_movie

import id.syaafiqi.movie_db_app.BuildConfig
import id.syaafiqi.movie_db_app.utils.Constants
import id.syaafiqi.movie_db_app.utils.Utility

data class MovieReviewModel(
    var profileImageUrl: String,
    val author: String,
    val content: String
) {
    object Map {
        fun from(data: ReviewResponse.Review) = MovieReviewModel(
            author = data.author.username,
            content = data.content,
            profileImageUrl = BuildConfig.IMAGE_BASE_URL + data.author.avatarPath
        )
    }
}