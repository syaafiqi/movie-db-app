package id.syaafiqi.movie_db_app.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import id.syaafiqi.movie_db_app.utils.Constants

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction {
        add(frameId, fragment)
    }
}

fun AppCompatActivity.replaceFragment(
    fragment: Fragment,
    frameId: Int,
    addBackStack: Boolean = true
) {
    supportFragmentManager.inTransaction {
        if (addBackStack) addToBackStack(Constants.POP_BACK_STACK_STATE)
        replace(frameId, fragment)
    }
}

fun AppCompatActivity.popReplaceFragment(
    fragment: Fragment,
    frameId: Int,
    addBackStack: Boolean = true
) {
    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    replaceFragment(fragment, frameId, addBackStack)
}

fun AppCompatActivity.popAllBackStack() {
    supportFragmentManager.popBackStack(
        Constants.POP_BACK_STACK_STATE,
        FragmentManager.POP_BACK_STACK_INCLUSIVE
    )
}