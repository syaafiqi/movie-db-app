package id.syaafiqi.movie_db_app.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import id.syaafiqi.movie_db_app.R
import id.syaafiqi.movie_db_app.utils.Utility

internal fun View.hideKeyboard() {
    val inputMethodManager: InputMethodManager =
        context.applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
}

internal fun View.show() {
    visibility = View.VISIBLE
}

internal fun View.hide() {
    visibility = View.GONE
}

internal fun View.invisible() {
    visibility = View.INVISIBLE
}

internal fun ImageView.load(url: String) {
    Glide.with(this.context).load(url).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .placeholder(Utility.createLoaderPlaceholder(this.context)).error(R.drawable.ic_baseline_image_24)
        .into(this)
}

internal fun View.showToast(any: Any) =
    Toast.makeText(context, any as String, Toast.LENGTH_SHORT).show()